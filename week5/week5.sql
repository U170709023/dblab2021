# 1. Show the films whose budge is greater than 10 million$ and ranking is less than 6.
select title from movies where budget > 10000000 and ranking < 6;
# 2. Show the action films whose rating is greater than 8.8 and produced after 2009.
select title from movies join genres on movies.movie_id = genres.movie_id
where rating > 8.8 and year < 2009 and genre_name = "Action";
# 3. Show the drama films whose duration is more than 150 minutes and oscars is more than 2.
select title from movies join genres on movies.movie_id = genres.movie_id
where duration >150 and oscars >2 and genre_name = "Drama";